package com.torneosfubol5.personas;


import javax.swing.*;

public class director {
    private String primernombre;
    private String segundonombre;
    private String primerapellido;
    private String segundoapellido;
    private String equipo;

    public director(){
        primernombre = JOptionPane.showInputDialog("Escriba el primer nombre para el tecnico");
        segundonombre = JOptionPane.showInputDialog("Escriba el segundo nombre para el tecnico");
        primerapellido = JOptionPane.showInputDialog("Escriba el primer apellido para el tecnico");
        segundoapellido = JOptionPane.showInputDialog("Escriba el segundo apellido para el tecnico");
        equipo = JOptionPane.showInputDialog("Escriba un nombre para el equipo wue dirige el tecnico");
    }

    public String getPrimernombre() {

        return primernombre;
    }

    public void setPrimernombre(String primernombre) {
        this.primernombre = primernombre;


    }

    public String getSegundonombre() {
        return segundonombre;
    }

    public void setSegundonombre(String segundonombre) {
        this.segundonombre = segundonombre;
    }

    public String getPrimerapellido() {
        return primerapellido;
    }

    public void setPrimerapellido(String primerapellido) {
        this.primerapellido = primerapellido;
    }

    public String getSegundoapellido() {
        return segundoapellido;
    }

    public void setSegundoapellido(String segundoapellido) {
        this.segundoapellido = segundoapellido;
    }

    public String getEquipo() {
        return equipo;
    }

    public void setEquipo(String equipo) {
        this.equipo = equipo;
    }
}
