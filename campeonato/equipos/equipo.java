package com.torneosfubol5.equipos;

public class equipo {
    private String nombreequipo;
    private int numerojugadores;
    private String nombredirector;
    private String apellidodirector;

    public equipo() {

        nombreequipo = "";
        numerojugadores = 11;
        nombredirector = "";
        apellidodirector = "";

    }

    public String getNombreequipo() {
        return nombreequipo;
    }

    public void setNombreequipo(String nombreequipo) {
        this.nombreequipo = nombreequipo;
    }

    public int getNumerojugadores() {
        return numerojugadores;
    }

    public void setNumerojugadores(int numerojugadores) {
        this.numerojugadores = numerojugadores;
    }

    public String getNombredirector() {
        return nombredirector;
    }

    public void setNombredirector(String nombredirector) {
        this.nombredirector = nombredirector;
    }

    public String getApellidodirector() {
        return apellidodirector;
    }

    public void setApellidodirector(String apellidodirector) {
        this.apellidodirector = apellidodirector;
    }
}
